// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCuCRtKupi-6SduFENcGWxJG3f-9nyGrHI',
    authDomain: 'collectioner-23dcd.firebaseapp.com',
    databaseURL: 'https://collectioner-23dcd.firebaseio.com',
    projectId: 'collectioner-23dcd',
    storageBucket: 'collectioner-23dcd.appspot.com',
    messagingSenderId: '453944562878'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
