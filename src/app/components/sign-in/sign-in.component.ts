import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl(),
    password: new FormControl()
  });

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authService
      .signIn(
        this.form.controls.email.value,
        this.form.controls.password.value
      )
      .then((credential) => {
        const uid = credential.user.uid;
        this.router.navigate([`/profile/${uid}`]);
      })
      .catch((error) => {
        console.error(error);
      });
  }

}
