import { Component, OnInit } from '@angular/core';
import { UserService, User } from 'src/app/services/user.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { SubscriptionService } from 'src/app/services/subscription.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  uid: string;
  authUid: string;
  user$: Observable<User>;

  constructor(
    public authService: AuthService,
    private userService: UserService,
    private subscriptionService: SubscriptionService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.uid = this.route.snapshot.params.id;
    this.authService.authState$.subscribe((user) => this.authUid = user.uid);
    this.user$ = this.userService.getUser(this.uid).valueChanges();
  }

  onSubscribe() {
    const subscription = { followerUid: this.authUid, followingUid: this.uid };
    this.subscriptionService.createSubscription(subscription);
  }

  onUnsubscribe() {
    const subscription = { followerUid: this.authUid, followingUid: this.uid };
    this.subscriptionService.deleteSubscription(subscription);
  }

}
