import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  form = new FormGroup({
    displayName: new FormControl(),
    profileType: new FormControl(),
    email: new FormControl(),
    password: new FormControl()
  });

  constructor(
    private authSerice: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.authSerice
      .signUp({
        displayName: this.form.controls.displayName.value,
        profileType: this.form.controls.profileType.value,
        email: this.form.controls.email.value,
        password: this.form.controls.password.value
      })
      .then((credential) => {
        const uid = credential.user.uid;
        this.router.navigate([`/profile/${uid}`]);
      })
      .catch((error) => {
        console.error(error);
      });
  }

}
