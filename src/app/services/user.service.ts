import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

export interface User {
  displayName: string;
  profileType: 'collectioner' | 'museum';
  email: string;
  photoURL: string | null;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private afs: AngularFirestore) { }

  getUser(uid: string): AngularFirestoreDocument<User> {
    return this.afs.collection<User>('users').doc(uid);
  }

}
