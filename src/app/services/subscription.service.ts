import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { query } from '@angular/core/src/render3';

export interface Subscription {
  followerUid: string;
  followingUid: string;
}

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {

  constructor(private afs: AngularFirestore) { }

  async createSubscription(subscription: Subscription) {
    await this.afs.collection<Subscription>('subscriptions').add(subscription);
  }

  async deleteSubscription({ followerUid, followingUid }: Subscription) {
    const batch = this.afs.firestore.batch();
    const query = await this.afs.collection<Subscription>('subscriptions', ref => ref
    .where('followerUid', '==', followerUid)
    .where('followingUid', '==', followingUid)
    ).ref.get();
    query.forEach(doc => batch.delete(doc.ref));
    batch.commit();
  }

}
