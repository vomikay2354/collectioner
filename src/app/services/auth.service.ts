import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

interface Credential {
  displayName: string;
  profileType: 'collectioner' | 'museum';
  email: string;
  password: string;
  photoURL?: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState$: Observable<firebase.User>;

  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore) {
    this.authState$ = this.afAuth.authState;
  }
  
  async signIn(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }

  async signUp({ displayName, profileType, email, password, photoURL }: Credential): Promise<firebase.auth.UserCredential> {
    const credential = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);
    await this.afs
      .collection('users')
      .doc(credential.user.uid)
      .set({ displayName, profileType, email, photoURL: photoURL || null });
    return Promise.resolve(credential);
  }

}
